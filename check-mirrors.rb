#!/usr/bin/env ruby

require 'English'
require 'cgi'
require 'ipaddr'
require 'json'
require 'net/http'
require 'net/https'
require 'nokogiri'
require 'pathname'
require 'resolv'
require 'set'
require 'tmpdir'
require 'timeout'
require 'uri'

$LOAD_PATH.unshift("#{File.dirname(__FILE__)}/lib")
require 'check-mirrors/optparse'
require 'check-mirrors/speed'

FALLBACK_DNS_POOL_NAME = 'dl.amnesia.boum.org'.freeze

BASE_URL = URI.parse("http://#{FALLBACK_DNS_POOL_NAME}/tails/")

TAILS_WEBSITE_URL = URI.parse('https://tails.boum.org/')

TMPDIR = ENV['TMPDIR'] || ENV['PWD']

NOKOGIRI_OPTIONS = Nokogiri::XML::ParseOptions::STRICT \
                   | Nokogiri::XML::ParseOptions::RECOVER \
                   | Nokogiri::XML::ParseOptions::NONET

DEFAULT_WGET_ARGS = [
  '--max-redirect=0',
  '--tries=2',
  '--server-response',
  '--timeout=60',
].freeze

DESIRED_FILES = {
  iso:     {
    ext:  '.iso',
    desc: 'ISO image',
  },
  iso_sig: {
    ext:  '.iso.sig',
    desc: 'OpenPGP signature of ISO image',
  },
  img:     {
    ext:  '.img',
    desc: 'USB image',
  },
  img_sig: {
    ext:  '.img.sig',
    desc: 'OpenPGP signature of USB image',
  },
}.freeze

ACCEPTABLE_CONTENT_TYPE = [
  'application/octet-stream',
  'application/x-iso9660-image',
].freeze

def error(*args)
  message = if args.length == 1
              args.pop
            else
              "[#{args.shift}] ".ljust(18) + args.join(' ')
            end
  if OPTIONS.debug
    debug('error', message)
  else
    puts message
  end
end

def error_and_exit(exit_code, *args)
  error(*args)
  exit(exit_code)
end

def debug(flag = nil, *message)
  return unless OPTIONS.debug

  if flag.nil?
    puts if message.empty?
  elsif message.empty?
    puts flag
  else
    puts "#{flag}:".ljust(10) + message.join(' ')
  end
end

def execute(*argv)
  debug('system', argv.join(' '))
  output = `LC_ALL=C #{argv.join(' ')} 2>&1`
  [$CHILD_STATUS, output]
end

def error_block(host, output)
  output.each_line { |line| error(host, line) }
end

def debug_block(output)
  output.each_line { |line| debug('debug', line) }
end

def download_headers?(wget_output)
  (/^\s+Content-Type:\s*(?<content_type>\S+)\s*$/i =~ wget_output \
      && ACCEPTABLE_CONTENT_TYPE.member?(content_type)) \
  || /^\s+X-Content-Type-Options:\s*nosniff\s*$/i !~ wget_output
end

def wget(mirror, uri, wget_args: [], is_download: true)
  raise 'wget_args must be an Array' unless wget_args.is_a?(Array)

  status, output = execute('wget', *DEFAULT_WGET_ARGS, uri.to_s, *wget_args)
  output.gsub!(/^\s+\d+K [ .]+ [\d%MK.=ms ]+$/, '')
  # Follow redirections
  if /^Location:\s*(?<uri>.+) \[following\]\s*$/i =~ output
    debug_block(output)
    output = wget(
      mirror, uri,
      # Strip the "Host: " header used for DNS pool mirrors
      wget_args:   wget_args.reject { |arg| arg =~ /^--header=['"]Host:/i },
      is_download: is_download
    )
  elsif !/^\s+Accept-Ranges:\s*bytes\s*$/i.match(output)
    error_block(mirror, "Does not support Range requests:\n#{output}")
  elsif is_download && !download_headers?(output)
    error_block(mirror, "Incorrect content type headers:\n#{output}")
  else
    status.exitstatus.zero? ? debug_block(output) : error_block(mirror, output)
  end
  output
end

def report_speed(output)
  saved_match = /^\s+\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} \((.+)\) - /
                .match(output)
  begin
    Speed.new(saved_match[1])
  rescue NoMethodError
    # Don't crash on download error
  end
end

def http_client(host, port)
  http = Net::HTTP.new(host, port)
  http.use_ssl = true if port == 443
  http.verify_mode = OpenSSL::SSL::VERIFY_PEER
  http.ca_path = '/etc/ssl/certs/'
  http
end

def http_request(uri, server: uri.host)
  debug('fetch', uri)
  req = Net::HTTP::Get.new(uri.request_uri)
  req['Host'] = uri.host
  begin
    res = http_client(server, uri.port).request(req)
  # TODO: in 2013, Timeout::Error did not inherit from StandardError;
  # this seems to have changed, so we should try
  # reverting 8b5420da725ca5464f5e1e75cb46b246f5ba1294;
  # then we can re-enable Lint/ShadowedException.
  # rubocop:disable Lint/ShadowedException
  rescue StandardError, SystemStackError, Timeout::Error
    error(server, $ERROR_INFO.to_s)
    return ''
  end
  # rubocop:enable Lint/ShadowedException
  http_response_body(res: res, server: server, uri: uri)
end

def http_response_body(res:, server:, uri:)
  case res
  when Net::HTTPSuccess
    res.body
  when Net::HTTPRedirection
    location = res['location']
    debug('redirected', location)
    http_request(URI.parse(location), server: URI.parse(location).host)
  else
    error(server, uri)
    ''
  end
end

def latest_version
  body = http_request(URI.parse('https://tails.boum.org/inc/stable_amd64_version/'))
  begin
    version = %r{<section id="content" role="main">\s*([\d.]+)\s*</section>}
              .match(body)[1]
    "tails-amd64-#{version}"
  rescue StandardError
    nil
  end
end

def scan_for_links(body, regexp)
  doc = Nokogiri::HTML(body) do |config|
    config.options = NOKOGIRI_OPTIONS
  end
  doc.css('a')
     .reject { |link| link['href'].nil? }
     .map { |link| Pathname.new(CGI.unescape(link['href'])).basename.to_s }
     .select { |filename| regexp =~ filename }
     .uniq
end

# check that the mirror has the versioned directory we want
def check_versions(mirror, base_dir_uri)
  body = http_request(base_dir_uri, server: server(mirror))
  versions = scan_for_links(body, /^tails-.+$/)
  if versions.empty?
    error(mirror, 'No version available.')
    return false
  end
  unless versions.include?(DESIRED_VERSION)
    error(mirror,
          "Could not find '#{DESIRED_VERSION}'. " \
          "Available versions: #{versions.join(', ')}.")
    return false
  end
  if (versions.length > 1) && !OPTIONS.allow_multiple
    error(mirror,
          "More than one version available: #{versions.sort.join(' ')}.")
  end
  true
end

def desired_trace
  if OPTIONS.desired_trace
    trace = OPTIONS.desired_trace
  else
    uri = URI.parse('https://tails.boum.org/inc/trace')
    body = http_request(uri)
    if !body.nil?
      trace = Integer(body)
      debug('trace', trace)
    else
      error_and_exit(1, "Could not fetch the trace from Tails' website.")
    end
  end
  trace
end

def ip?(mirror)
  begin
    IPAddr.new(mirror)
  rescue StandardError
    return false
  end
  true
end

def base_url(mirror)
  ip?(mirror) ? BASE_URL.to_s : mirror
end

def server(mirror)
  ip?(mirror) ? mirror : URI.parse(mirror).host
end

def check_trace(mirror, trace)
  body = http_request(
    URI.parse("#{base_url(mirror)}/project/trace"),
    server: server(mirror)
  )
  begin
    epoch = Integer(body)
    if epoch == trace
      debug('trace', body)
    elsif epoch < trace
      error(mirror, "Trace set in the past: #{epoch} should be #{trace}.")
      return false
    elsif epoch > trace
      error(mirror, "Trace set in the future: #{epoch} should be #{trace}.")
      return false
    end
  rescue ArgumentError
    error(mirror, "Trace is not an integer: #{epoch}")
    return false
  end
  true
end

def links_found_in_html(body)
  Nokogiri::HTML(body) { |config| config.options = NOKOGIRI_OPTIONS }
          .css('a')
          .reject { |link| link['href'].nil? }
          .map { |link| Pathname.new(CGI.unescape(link['href'])).basename.to_s }
end

def filenames_found_on_mirror(mirror, desired_version_dir_uri)
  body = http_request(desired_version_dir_uri, server: server(mirror))
  filenames = {}
  links_found_in_html(body).each do |linked_filename|
    DESIRED_FILES.each do |filetype, fileinfo|
      filenames[filetype] = linked_filename \
        if linked_filename == "#{DESIRED_VERSION}#{fileinfo[:ext]}"
    end
  end
  filenames
end

# Check that the versioned directory contains all the desired files
def check_files_listing(mirror, desired_version_dir_uri)
  filenames = filenames_found_on_mirror(mirror, desired_version_dir_uri)
  missing_filetypes = Set.new(DESIRED_FILES.keys) - Set.new(filenames.keys)
  return filenames if missing_filetypes.empty?

  missing_files_desc = missing_filetypes.to_a.map do |filetype|
    DESIRED_FILES[filetype][:desc]
  end.join(', ')
  error(
    mirror,
    "Missing files in #{desired_version_dir_uri}: #{missing_files_desc}."
  )
  false
end

# Delete possible dangling directories from previous runs that crashed
def delete_old_temporary_directories(tmpdir)
  Dir.glob("#{tmpdir}/check-mirrors-*").each do |dangling|
    FileUtils.remove_entry dangling
  end
end

def check_speed(ip, speed)
  return unless speed && (speed.values.min < OPTIONS.speed)

  error(ip, "Slow mirror: #{speed.values.min}")
end

def wget_args(filetype, filenames, tmpdir)
  args = []
  args.push('--spider') if OPTIONS.fast
  args += if OPTIONS.keep && [:iso, :img].member?(filetype)
            ['--no-clobber']
          else
            ['-O', File.join(tmpdir, filenames[filetype])]
          end
  args
end

def download(mirror, desired_version_dir_uri, filenames, tmpdir)
  speed = {}
  DESIRED_FILES.each do |filetype, _fileinfo|
    uri = (desired_version_dir_uri + filenames[filetype]).to_s
    wget_args = wget_args(filetype, filenames, tmpdir)
    if ip?(mirror)
      # Ensure we query the right DNS pool mirror,
      # while still passing it the correct Host header
      uri.gsub!(BASE_URL.to_s, "http://#{mirror}/tails/")
      wget_args.push("--header='Host: #{FALLBACK_DNS_POOL_NAME}'")
    end
    debug('download', uri)
    if [:iso, :img].member?(filetype)
      speed[filetype] = report_speed(
        wget(
          mirror, uri, wget_args: wget_args, is_download: true
        )
      )
      debug('speed', speed[filetype])
    else
      wget(mirror, uri, wget_args: wget_args, is_download: false)
    end
  end
  speed
end

def verify_signatures(mirror, filenames, tmpdir)
  verify_signature(mirror,
                   File.join(tmpdir, filenames[:iso_sig]),
                   File.join(tmpdir, filenames[:iso]))
  verify_signature(mirror,
                   File.join(tmpdir, filenames[:img_sig]),
                   File.join(tmpdir, filenames[:img]))
end

def verify_signature(mirror, signature, signed_file)
  status, output = execute('gpg', '--verify', signature, signed_file)
  if status != 0
    error_block(mirror, output)
  else
    debug_block(output)
  end
end

def parse_mirrors_json
  JSON.parse(
    if OPTIONS.mirrors_json_file
      File.read(OPTIONS.mirrors_json_file)
    else
      http_request(URI.parse("#{TAILS_WEBSITE_URL}/mirrors.json"))
    end
  )
end

def mirrors_url_prefixes(json)
  json['mirrors']
    .select { |mirror| mirror['weight'].positive? }
    .map { |mirror| mirror['url_prefix'] }
end

def read_mirrors_with_failures
  if OPTIONS.ignore_failures
    JSON.parse(File.read(OPTIONS.ignore_failures)).keys
  else
    []
  end
end

def write_mirrors_with_failures(mirrors)
  File.write(OPTIONS.store_failures, JSON.dump(mirrors)) if OPTIONS.store_failures
end

def get_mirror_weight(json, url_prefix)
  json['mirrors']
    .select { |mirror| mirror['url_prefix'] == url_prefix }
    .map { |mirror| mirror['weight'] }
    .reject(&:nil?)
    .sum
end

def get_mirrors_weight(url_prefix_list, mirrors_json)
  url_prefix_list
    .map { |u| get_mirror_weight(mirrors_json, u) }
    .sum
end

def percentage(good, bad)
  ((good / (good + bad).to_f) * 100).to_i
end

def output_stats(good, bad, mirrors_json)
  perc = percentage(good.length, bad.length)
  puts "Good=#{good.length} (#{perc}%)"
  puts "Bad=#{bad.length}"
  weighted_good = get_mirrors_weight(good, mirrors_json)
  total_weight = mirrors_json['mirrors']
                 .map { |mirror| mirror['weight'] }
                 .sum
  weighted_bad = total_weight - weighted_good
  weighted_perc = percentage(weighted_good, total_weight - weighted_good)
  puts 'Weighted:'
  puts "  Good=#{weighted_good} (#{weighted_perc}%)"
  puts "  Bad=#{weighted_bad}"
  puts "  TotalWeight=#{total_weight}"
end

### Main

OPTIONS = OptparseCheckMirrors.new.parse(ARGV)

mirrors_json = parse_mirrors_json

if OPTIONS.url_prefix
  url_prefixes = [OPTIONS.url_prefix]
  ips = []
elsif OPTIONS.ip
  url_prefixes = []
  ips = [OPTIONS.ip]
else
  url_prefixes = mirrors_url_prefixes(mirrors_json)
  ips = Resolv.getaddresses(FALLBACK_DNS_POOL_NAME)
end

if OPTIONS.ip && OPTIONS.url
  system('curl --verbose --connect-timeout 30 ' \
         "--header 'Host: #{FALLBACK_DNS_POOL_NAME}' " \
         "#{OPTIONS.url.gsub(FALLBACK_DNS_POOL_NAME, OPTIONS.ip)} " \
         '| w3m -dump -T text/html')
  exit
end

desired_version = ARGV.shift
if desired_version
  DESIRED_VERSION = desired_version
else
  if OPTIONS.channel != 'stable'
    error_and_exit(1,
                   'When a non default channel is used you must specify ' \
                   'the release to check explicitly via the RELEASE ' \
                   'positional parameter.')
  end
  DESIRED_VERSION = latest_version
end
error_and_exit(1, "Could not fetch the latest version from Tails' website.") \
  if DESIRED_VERSION.nil?

debug('version', DESIRED_VERSION)

error_and_exit(1, "Excess arguments: #{ARGV.join(' ')}") unless ARGV.empty?

trace = desired_trace

to_ignore = read_mirrors_with_failures
failing_mirrors = {}
good_mirrors = []
(url_prefixes + ips).each do |mirror|
  debug
  debug('mirror', mirror)

  if to_ignore.include? mirror
    error(mirror, 'Ignoring, mirror marked as failed')
    next
  end

  mirror = mirror.to_s
  base_dir_uri = URI.parse(base_url(mirror) + "/#{OPTIONS.channel}/")
  desired_version_dir_uri = URI.parse(base_dir_uri.to_s + "#{DESIRED_VERSION}/")

  unless check_trace(mirror, trace)
    failing_mirrors[mirror] = 'check_trace'
    next
  end

  unless check_versions(mirror, base_dir_uri)
    failing_mirrors[mirror] = 'check_versions'
    next
  end

  filenames = check_files_listing(mirror, desired_version_dir_uri)
  unless filenames
    failing_mirrors[mirror] = 'check_files_listing'
    next
  end

  good_mirrors.push(mirror)

  delete_old_temporary_directories(TMPDIR)

  # the desired files appear to be on the mirror, let's download and check them
  Dir.mktmpdir('check-mirrors-', TMPDIR) do |tmpdir|
    speed = download(mirror, desired_version_dir_uri, filenames, tmpdir)
    verify_signatures(mirror, filenames, tmpdir) unless OPTIONS.fast
    check_speed(mirror, speed) if OPTIONS.speed
  end

  debug
end
write_mirrors_with_failures(failing_mirrors)
output_stats(good_mirrors, failing_mirrors, mirrors_json) if OPTIONS.stats
