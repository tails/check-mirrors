# Option parser for check-mirrors.rb
require 'optparse'
require 'ostruct'

# Option parser for check-mirrors.rb
class OptparseCheckMirrors
  DEFAULT_CHANNEL = 'stable'.freeze

  # Define supported options
  class ScriptOptions
    attr_accessor :allow_multiple,
                  :channel,
                  :debug,
                  :desired_trace,
                  :fast,
                  :ignore_failures,
                  :ip,
                  :keep,
                  :stats,
                  :mirrors_json_file,
                  :speed,
                  :store_failures,
                  :url,
                  :url_prefix

    def initialize
      self.allow_multiple = false
      self.channel = DEFAULT_CHANNEL
      self.debug = false
      self.fast = false
      self.keep = false
      self.stats = false
    end

    def define_options(parser)
      parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} " \
                      '[options] [RELEASE]'
      parser.separator ''
      parser.separator "Options that determine which mirror(s) we'll test:"
      mirror_selection_options(parser)
      parser.separator ''
      parser.separator "Options that determine which URL we'll check:"
      url_selection_options(parser)
      parser.separator ''
      parser.separator "Options that determine what kind of check we'll do:"
      checks_selection_options(parser)
      parser.separator ''
      parser.separator 'Options that determine how failures should be handled:'
      failures_selection_options(parser)
      parser.separator ''
      parser.separator 'Other options:'
      parser.on('-d', '--debug') { self.debug = true }
      parser.on(
        '-k', '--keep',
        'Keep the ISO and USB images and their signature after download.',
        'Default: delete them'
      ) { self.keep = true }
      parser.on(
        '--stats',
        'Report stats about number of valid mirrors',
        'Default: only print errors'
      ) { self.stats = true }
    end

    def mirror_selection_options(parser)
      parser.on(
        '--url-prefix URL_PREFIX',
        'Only check the mirror that has the specified URL prefix'
      ) { |url_prefix| self.url_prefix = url_prefix }
      parser.on('-i', '--ip IP', 'Only check the specified IP') do |ip|
        if /\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}/.match(ip)
          self.ip = ip
        else
          puts 'Wrong IP format for option --ip. Ignoring it.'
        end
      end
      parser.on(
        '--mirrors-json-file FILE',
        'Use the specified local mirrors.json file.',
        'Default: retrieve it from our website'
      ) { |mirrors_json_file| self.mirrors_json_file = mirrors_json_file }
    end

    def url_selection_options(parser)
      parser.on(
        '-c', '--channel NAME',
        'Specify which channel to check for versions on.',
        "Default: '#{DEFAULT_CHANNEL}'",
        'If you set the channel to anything else,',
        'then you must set the RELEASE positional argument'
      ) do |channel|
        self.channel = channel
      end
      parser.on(
        '-u', '--url URL',
        'Fetch the given URL from the corresponding IP',
        '(used in combination with the --ip option).',
        'Requires curl and w3m'
      ) do |url|
        self.url = url
      end
    end

    def checks_selection_options(parser)
      parser.on(
        '-f', '--fast',
        'Check that ISO and USB images are present, without downloading them'
      ) { self.fast = true }
      parser.on(
        '-a', '--allow-multiple',
        'Suppress warning when multiple releases are found'
      ) { self.allow_multiple = true }
      parser.accept(Speed) { |speed| Speed.new(speed) }
      parser.on(
        '-s', '--minimum-speed SPEED', Speed,
        'Report an error if an image download was slower than SPEED.',
        "Example: --minimum-speed \'1.0 MB/s\'"
      ) { |speed| self.speed = speed }
      parser.on(
        '-t', '--trace TRACE', OptionParser::DecimalInteger,
        'Check that the mirror(s) serve the specified version',
        'of the mirrored data set.',
        'Default: retrieve it from our website'
      ) { |trace| self.desired_trace = trace }
    end

    def failures_selection_options(parser)
      parser.on(
        '--ignore-failures FILE',
        'Specify a file listing mirrors to ignore (known to have failures)'
      ) { |ignore| self.ignore_failures = ignore }
      parser.on(
        '--store-failures FILE',
        'Specify a file where to store mirrors with failures'
      ) { |store| self.store_failures = store }
    end
  end

  #
  # Return a structure describing the options.
  #
  def parse(args)
    # The options specified on the command line will be collected in
    # *options*.

    @options = ScriptOptions.new
    @args = OptionParser.new do |parser|
      @options.define_options(parser)
      parser.parse!(args)
    end
    assert_no_conflicting_options(@options)
    @options
  end

  def assert_no_conflicting_options(options)
    raise '--ip and --url-prefix cannot be used together' \
      if options.ip && options.url_prefix
    raise '--fast and --minimum-speed cannot be used together' \
      if options.fast && options.speed
    raise '--ignore-failures and --store-failures cannot be used together' \
      if options.ignore_failures && options.store_failures
  end

  attr_reader :parser, :options
end
