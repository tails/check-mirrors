# Speed of data transfer, as in "NUMBER UNIT/s"
class Speed < Numeric
  attr_reader :value

  def initialize(string)
    super()
    @string = string
    match = %r{([\d.,]+) (.+)/s}.match(@string)
    @number = match[1].to_f
    @unit = match[2]
    case @unit
    when 'B'
      @value = @number
    when 'KB'
      @value = @number * 1024
    when 'MB'
      @value = @number * 1024 * 1024
    end
    @value = @value.to_i
  end

  def <=>(other)
    @value <=> other.value
  end

  def to_s
    @string
  end
end
