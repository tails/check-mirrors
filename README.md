# Requirements

	sudo apt install curl ruby-nokogiri wget

# Help

	./check-mirrors.rb --help

# Check one mirror

	./check-mirrors.rb --fast --url-prefix https://tails.osuosl.org/

# Check all mirrors

	./check-mirrors.rb --fast
